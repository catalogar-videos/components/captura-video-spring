# Imagem base Alpine Linux
FROM alpine:latest

# Usuário non-root
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

RUN apk update && apk add gcompat

# Copia o binário para a imagem
COPY ./target/captura-video-spring /home/appuser/app

# Define o diretório de trabalho
WORKDIR /home/appuser

# Executa o binário como usuário non-root
USER appuser

# Comando para iniciar o aplicativo
ENTRYPOINT ["./app"]

package br.knin.project.captura.video.sources;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.repository.Factory;
import lib.captura.video.core.source.VideoSourceEntry;

import java.util.Optional;
import java.util.function.Function;

public enum VideoSourceType {

    Spa("SPA"),
    N("N");

    VideoSourceType(final String value) {
        this.value = value;
    }

    private final String value;

    Optional<VideoSourceRepository.Entity> findEntityByAlias(final VideoSourceRepository repository) {
        return repository.findByAlias(value);
    }

    public Factory<VideoSourceEntry> createVideoSourceFactory
            (
                    final Function<? super Channel, ? extends VideoSourceEntry> function
            ) {

        return new Factory<>() {

            @Override
            public VideoSourceEntry build(final Channel channel) {
                return function.apply(channel);
            }

            @Override
            public String name() {
                return value;
            }

        };

    }
}

package br.knin.project.captura.video.adapter.html;

import br.knin.project.captura.video.sources.HtmlDocument;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class HtmlDocumentImpl implements HtmlDocument {

    private static <T> Stream<T> stream(final Iterator<T> node) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(node, Spliterator.ORDERED), false);
    }

    public HtmlDocumentImpl(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private final ObjectMapper objectMapper;

    @SneakyThrows
    @Override
    public Document fetchDocument(final String url) {
        return Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64)")
                .header("Accept-Language", "en-US,en;q=0.5")
                .header("Referer", "https://google.com")
                .header("Connection", "keep-alive")
                .get();
    }

    @Override
    public Stream<Map.Entry<String, List<String>>> parseElements(final Elements elements, final Pattern pattern) {
        return elements.stream()
                .map(Element::data)
                .map(pattern::matcher)
                .filter(Matcher::find)
                .map(matcher -> matcher.group(1))
                .map(s -> s.replace("'", "\""))
                .map(this::parseJsonToMapList)
                .flatMap(m -> m.entrySet().stream())
                .filter(Predicate.not(entry -> entry.getValue().isEmpty()));
    }

    private Map<String, List<String>> parseJsonToMapList(final String jsonString) {

        try {

            final JsonNode jsonNode = objectMapper.readTree(jsonString);

            return stream(jsonNode.fields())
                    .filter(e -> e.getValue().isArray())
                    .collect
                            (
                                    Collectors.toMap
                                            (
                                                    Map.Entry::getKey,
                                                    e -> stream(e.getValue().iterator())
                                                            .map(JsonNode::textValue)
                                                            .toList()
                                            )
                            );

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

}

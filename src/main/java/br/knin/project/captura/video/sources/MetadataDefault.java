package br.knin.project.captura.video.sources;

import lib.captura.video.core.storage.Metadata;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

final class MetadataDefault implements Metadata {

    MetadataDefault(final Map<String, String> parameters) {
        this.parameters = parameters;
    }

    private final Map<String, String> parameters;

    @Override
    public String name() {
        return parameters.get("url").split("/")[5];
    }

    @Override
    public String author() {
        return parameters.get("author");
    }

    @Override
    public Collection<String> tags() {
        return Arrays.stream(parameters.get("tags").split(",")).toList();
    }

    @Override
    public Optional<LocalDate> createDate() {
        return parameters.containsKey("date") ? Optional.of(LocalDate.parse(parameters.get("date"))) : Optional.empty();
    }
}

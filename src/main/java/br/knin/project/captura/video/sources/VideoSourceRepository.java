package br.knin.project.captura.video.sources;

import java.util.Collection;
import java.util.Optional;

public interface VideoSourceRepository {

    Optional<Entity> findByAlias(final String name);

    interface Entity {

        Collection<String> getRequiredParameters();

        Collection<String> getTemplates();

    }

}

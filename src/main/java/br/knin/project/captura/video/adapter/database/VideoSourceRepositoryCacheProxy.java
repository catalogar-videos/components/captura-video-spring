package br.knin.project.captura.video.adapter.database;

import br.knin.project.captura.video.sources.VideoSourceRepository;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.SneakyThrows;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public final class VideoSourceRepositoryCacheProxy implements VideoSourceRepository {

    public VideoSourceRepositoryCacheProxy(final VideoSourceRepository repository) {
        cache = CacheBuilder
                .newBuilder()
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .build
                        (
                                new CacheLoader<>() {

                                    @SuppressWarnings("NullableProblems")
                                    @Override
                                    public Optional<Entity> load(final String key) {
                                        return repository.findByAlias(key);
                                    }
                                }
                        );

    }

    private final LoadingCache<String, Optional<Entity>> cache;

    @SneakyThrows
    @Override
    public Optional<Entity> findByAlias(final String name) {
        return cache.get(name);
    }

}

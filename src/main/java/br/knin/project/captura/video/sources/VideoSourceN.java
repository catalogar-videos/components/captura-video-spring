package br.knin.project.captura.video.sources;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.storage.Metadata;
import lib.captura.video.link.IHttpClient;
import lib.captura.video.link.Link;
import lib.captura.video.link.VideoSourceLinkCapture;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public final class VideoSourceN extends VideoSourceLinkCapture {

    public VideoSourceN
            (
                    final VideoSourceRepository repository,
                    final Channel channel,
                    final String directory,
                    final IHttpClient iHttpClient
            ) {
        super(channel, iHttpClient);
        this.repository = repository;
        this.directory = directory;
    }

    private final String directory;

    private final VideoSourceRepository repository;

    @Override
    protected Collection<Link> links(final Map<String, String> parameters) {
        return repository.findByAlias("N")
                .orElseThrow()
                .getTemplates()
                .stream()
                .map
                        (
                                template -> new Link(directory) {
                                    @Override
                                    protected String url() {
                                        return template.replace("{id}", parameters.get("url").split("/")[4]);
                                    }

                                    @Override
                                    protected String fileExtension() {
                                        return "mp4";
                                    }
                                }
                        )
                .collect(Collectors.toSet());
    }

    @Override
    protected Metadata buildMetadata(final Map<String, String> parameters) {
        return new MetadataDefault(parameters);
    }

    @Override
    public Collection<String> requiredParameters() {
        return VideoSourceType.N.findEntityByAlias(repository).orElseThrow().getRequiredParameters();
    }

}

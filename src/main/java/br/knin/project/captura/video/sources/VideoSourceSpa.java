package br.knin.project.captura.video.sources;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.storage.Metadata;
import lib.captura.video.link.IHttpClient;
import lib.captura.video.link.Link;
import lib.captura.video.link.VideoSourceLinkCapture;
import lombok.Builder;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class VideoSourceSpa extends VideoSourceLinkCapture {

    private VideoSourceSpa(final VideoSourceSpaBuilder videoSourceSpaBuilder) {
        super(videoSourceSpaBuilder.channel, videoSourceSpaBuilder.iHttpClient);
        this.videoSourceSpaBuilder = videoSourceSpaBuilder;
    }

    private final VideoSourceSpaBuilder videoSourceSpaBuilder;

    @Override
    protected Collection<Link> links(final Map<String, String> parameters) {

        final Document document = Objects.requireNonNull(videoSourceSpaBuilder.fetchDocument(parameters.get("url")));

        final Element body = document.body();

        final Element main = body.getElementsByTag("main").removeFirst();

        final Elements container = main.getElementsByAttributeValue("type", "text/javascript");

        return videoSourceSpaBuilder
                .streamByElements(container)
                .filter(Quality::contain)
                .map(videoSourceSpaBuilder::linkQuality)
                .sorted(LinkQuality::compareTo)
                .map(LinkQuality::createLink)
                .collect(Collectors.toSet());

    }

    @Override
    protected Metadata buildMetadata(final Map<String, String> parameters) {
        return new MetadataDefault(parameters);
    }

    @Override
    public Collection<String> requiredParameters() {
        return videoSourceSpaBuilder.findEntityByAlias().orElseThrow().getRequiredParameters();
    }

    private enum Quality {

        LOW("480p"),
        MEDIUM("720p"),
        HIGH("1080p");

        private static boolean contain(final Map.Entry<String, List<String>> entry) {
            return Arrays.stream(Quality.values()).map(quality -> quality.value)
                    .anyMatch(v -> v.equalsIgnoreCase(entry.getKey()));
        }

        private static Quality createQuality(final String value) {
            return Arrays.stream(Quality.values())
                    .filter(quality -> quality.value.equalsIgnoreCase(value))
                    .findFirst()
                    .orElseThrow();
        }

        Quality(final String value) {
            this.value = value;
        }

        private final String value;

    }

    private static final class LinkQuality implements Comparable<LinkQuality> {

        LinkQuality(final Map.Entry<String, List<String>> entry, final String directory) {
            this.quality = Quality.createQuality(entry.getKey());
            this.link = entry.getValue().getFirst();
            this.directory = directory;
        }

        private final String directory;

        private final Quality quality;

        private final String link;

        @Override
        public int compareTo(final LinkQuality o) {
            return o.quality.compareTo(quality);
        }

        private Link createLink() {
            return new Link(directory) {
                @Override
                protected String url() {
                    return link;
                }

                @Override
                protected String fileExtension() {
                    return "mp4";
                }
            };
        }

    }

    @Builder
    public static final class VideoSourceSpaBuilder {

        private static final Pattern PATTERN = Pattern.compile("var stream_data = (.*?);");

        private String directory;

        private Channel channel;

        private VideoSourceRepository repository;

        private HtmlDocument htmlDocument;

        private IHttpClient iHttpClient;

        public VideoSourceSpa createSpaVideoSource() {
            return new VideoSourceSpa(this);
        }

        private LinkQuality linkQuality(final Map.Entry<String, List<String>> entry) {
            return new LinkQuality(entry, directory);
        }

        private Optional<VideoSourceRepository.Entity> findEntityByAlias() {
            return VideoSourceType.Spa.findEntityByAlias(repository);
        }

        private Document fetchDocument(final String url) {
            return htmlDocument.fetchDocument(url);
        }

        private Stream<Map.Entry<String, List<String>>> streamByElements(final Elements script) {
            return htmlDocument.parseElements(script, PATTERN);
        }

    }

}

package br.knin.project.captura.video.adapter.database;

import br.knin.project.captura.video.sources.VideoSourceRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface VideoSourceSpringRepository extends MongoRepository<VideoSourceEntity, String>, VideoSourceRepository {
}

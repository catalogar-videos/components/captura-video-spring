package br.knin.project.captura.video.sources;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public interface HtmlDocument {

    Document fetchDocument(final String url);

    Stream<Map.Entry<String, List<String>>> parseElements(final Elements elements, final Pattern pattern);

    final class HtmlDocumentCache implements HtmlDocument {

        public HtmlDocumentCache(final HtmlDocument htmlDocument) {
            this.htmlDocument = htmlDocument;
            this.cache = new HashMap<>();
        }

        private final HtmlDocument htmlDocument;

        private final Map<String, Document> cache;

        @Override
        public Document fetchDocument(final String url) {
            return cache.computeIfAbsent(url, htmlDocument::fetchDocument);
        }

        @Override
        public Stream<Map.Entry<String, List<String>>> parseElements(final Elements elements, final Pattern pattern) {
            return htmlDocument.parseElements(elements, pattern);
        }

    }

}

package br.knin.project.captura.video;

import br.knin.project.captura.video.adapter.database.VideoSourceRepositoryCacheProxy;
import br.knin.project.captura.video.adapter.html.HtmlDocumentImpl;
import br.knin.project.captura.video.sources.HtmlDocument;
import br.knin.project.captura.video.adapter.message.RabbitConsumer;
import br.knin.project.captura.video.adapter.message.RabbitProducer;
import br.knin.project.captura.video.sources.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lib.captura.video.core.repository.Repository;
import lib.captura.video.link.IHttpClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.http.HttpClient;

@Configuration
class FactoryConfiguration {

    @Bean
    ObjectMapper objectMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    @Bean
    IHttpClient iHttpClient() {
        return HttpClient::newHttpClient;
    }

    @Bean
    HtmlDocument htmlDocument(final ObjectMapper objectMapper) {
        return new HtmlDocument.HtmlDocumentCache(new HtmlDocumentImpl(objectMapper));
    }

    @Bean
    Repository repository
            (
                    final VideoSourceRepository repository,
                    @Value("${captura.video.directory}") final String directory,
                    final IHttpClient iHttpClient,
                    final HtmlDocument htmlDocument
            ) {

        return Repository
                .create()
                .registerFactory
                        (
                                VideoSourceType.N.createVideoSourceFactory
                                        (
                                                channel -> new VideoSourceN
                                                        (
                                                                new VideoSourceRepositoryCacheProxy(repository),
                                                                channel,
                                                                directory,
                                                                iHttpClient
                                                        )
                                        )
                        )
                .registerFactory
                        (
                                VideoSourceType.Spa.createVideoSourceFactory
                                        (
                                                channel -> VideoSourceSpa.VideoSourceSpaBuilder
                                                        .builder()
                                                        .channel(channel)
                                                        .iHttpClient(iHttpClient)
                                                        .repository(repository)
                                                        .directory(directory)
                                                        .htmlDocument(htmlDocument)
                                                        .build()
                                                        .createSpaVideoSource()
                                        )
                        );
    }

    @Bean
    RabbitProducer rabbitProducer(final RabbitTemplate rabbitTemplate, final ObjectMapper objectMapper) {
        return new RabbitProducer(rabbitTemplate, objectMapper);
    }

    @Bean
    RabbitConsumer rabbitConsumer(final ObjectMapper objectMapper, final Repository repository, final RabbitProducer producer) {
        return new RabbitConsumer(objectMapper, repository, producer);
    }

}

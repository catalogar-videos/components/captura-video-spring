package br.knin.project.captura.video.adapter.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.repository.Repository;
import lib.captura.video.core.source.Video;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import java.util.Map;
import java.util.UUID;

public final class RabbitConsumer {

    private final Logger logger = LoggerFactory.getLogger(RabbitConsumer.class);

    public RabbitConsumer(final ObjectMapper objectMapper, final Repository repository, final RabbitProducer rabbitProducer) {
        this.objectMapper = objectMapper;
        this.repository = repository;
        this.rabbitProducer = rabbitProducer;
    }

    private final ObjectMapper objectMapper;

    private final Repository repository;

    private final RabbitProducer rabbitProducer;

    @RabbitListener(queues = "queue.captura.input")
    public void handleMessage(final String string) throws JsonProcessingException {
        final String id = UUID.randomUUID().toString();
        final Message message = objectMapper.readValue(string, Message.class);
        logger.info(message.toString());
        message.video(repository, m -> rabbitProducer.send(id, m))
                .write(raw -> rabbitProducer.send(id, raw));
    }

    @ToString
    @Setter
    static final class Message {

        private String videoSource;

        private Map<String, String> parameters;

        private Video video(final Repository repository, final Channel channel) {
            return repository.videoSource(videoSource, channel).video(parameters);
        }
    }

}

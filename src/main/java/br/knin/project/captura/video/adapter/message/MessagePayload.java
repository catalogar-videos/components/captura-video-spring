package br.knin.project.captura.video.adapter.message;

import lib.captura.video.core.channel.Information;
import lib.captura.video.core.channel.Message;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
final class MessagePayload {

    MessagePayload(final String id, final Message message) {
        this.id = id;
        this.message = message;
        timestamp = LocalDateTime.now();
    }

    @Getter
    private final LocalDateTime timestamp;

    @Getter
    private final String id;

    private final Message message;

    public String getEvent() {
        return message.event().name();
    }

    public Collection<InformationPayload> getInfos() {
        return message.infos().stream().map(InformationPayload::new)
                .collect(Collectors.toSet());
    }

    @SuppressWarnings("unused")
    static final class InformationPayload {

        private final Information information;

        private InformationPayload(final Information information) {
            this.information = information;
        }

        public String getKey() {
            return information.key();
        }

        public String getValue() {
            return information.value();
        }

    }
}

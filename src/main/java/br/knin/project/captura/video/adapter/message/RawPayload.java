package br.knin.project.captura.video.adapter.message;

import lib.captura.video.core.storage.Metadata;
import lib.captura.video.core.storage.Raw;
import lombok.Getter;

import java.time.LocalDate;
import java.util.Collection;

@SuppressWarnings("unused")
public class RawPayload {

    public RawPayload(final String id, final Raw raw) {
        this.id = id;
        this.raw = raw;
    }

    @Getter
    private final String id;

    private final Raw raw;

    public String getVideoFilePath() {
        return raw.videoFilePath();
    }

    public MetadataPayload getMetadata() {
        return new MetadataPayload(raw.metadata());
    }

    public static final class MetadataPayload {

        public MetadataPayload(final Metadata metadata) {
            this.metadata = metadata;
        }

        private final Metadata metadata;

        public String getName() {
            return metadata.name();
        }

        public String getAuthor() {
            return metadata.author();
        }

        public Collection<String> getTags() {
            return metadata.tags();
        }

        public LocalDate getCreateDate() {
            return metadata.createDate().orElse(LocalDate.now());
        }

    }
}

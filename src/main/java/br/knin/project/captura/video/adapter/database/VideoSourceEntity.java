package br.knin.project.captura.video.adapter.database;

import br.knin.project.captura.video.sources.VideoSourceRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Document("video_source")
public class VideoSourceEntity implements VideoSourceRepository.Entity {

    @Id
    private String id;

    private String name;

    private String type;

    private String alias;

    @Field("required_parameters")
    private Collection<String> requiredParameters;

    private Collection<String> templates;

}

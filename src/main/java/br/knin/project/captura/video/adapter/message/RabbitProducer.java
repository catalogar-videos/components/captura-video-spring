package br.knin.project.captura.video.adapter.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import lib.captura.video.core.channel.Message;
import lib.captura.video.core.storage.Raw;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public final class RabbitProducer {

    public RabbitProducer(final RabbitTemplate rabbitTemplate, final ObjectMapper objectMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = objectMapper;
    }

    private final RabbitTemplate rabbitTemplate;

    private final ObjectMapper objectMapper;

    @SneakyThrows
    void send(final String id, final Message message) {
        rabbitTemplate.convertAndSend
                (
                        "topic.captura.event",
                        "#",
                        objectMapper.writeValueAsBytes(new MessagePayload(id, message))
                );
    }

    @SneakyThrows
    public void send(final String id, final Raw raw) {
        rabbitTemplate.convertAndSend
                (
                        "topic.captura.event",
                        "output",
                        objectMapper.writeValueAsBytes(new RawPayload(id, raw))
                );
    }

}
